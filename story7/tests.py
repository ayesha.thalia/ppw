import time

from django.test import TestCase, Client
from django.urls import resolve
from . import views

# Create your tests here.

class LandingPageUnitTest(TestCase):
    def test_landing_page_found(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_template_story7(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, "story7.html")
    
    def test_func_page(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, views.story7)