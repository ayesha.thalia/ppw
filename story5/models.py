from django.db import models

# Create your models here.

class Matkul(models.Model):
    nama = models.CharField(max_length = 35)
    dosen = models.CharField(max_length = 35)
    jumlahsks = models.CharField(max_length = 35)
    deskripsi = models.CharField(max_length = 35)
    semestertahun = models.CharField(max_length = 35)
    ruangan = models.CharField(max_length = 35)

    def __str__(self):
        return self.nama
