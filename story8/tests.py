from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views
# Create your tests here.

class UnitTestStory8(TestCase) :
    def test_url_story8(self) :
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_template_story8(self) :
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')

    def test_func_page(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, views.story8)
    
    def test_book_api(self):
        found = resolve(reverse('story8:bookAPI'))
        self.assertTrue(found.func, views.bookAPI)
