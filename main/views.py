from django.http import HttpResponse
from django.shortcuts import render, redirect

def home(request):
    response = {}
    return render(request,"main/home.html", response)

def story1(request):
    response = {}
    return render(request, "main/story1.html", response)

def story3(request):
    response = {}
    return render(request, "main/story3.html", response)

def quotes(request):
    response = {}
    return render(request, "main/quotes.html", response)

def designs(request):
    response = {}
    return render(request, "main/designs.html", response)

