from django.contrib import admin

# Register your models here.
from story6.models import Kegiatan, Peserta

admin.site.register(Kegiatan)
admin.site.register(Peserta)

